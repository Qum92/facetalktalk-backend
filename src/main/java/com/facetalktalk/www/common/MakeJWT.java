package com.facetalktalk.www.common;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.facetalktalk.www.vo.UserVO;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MakeJWT {
	private static Date ISSUE_DATE;
	private static Date EXPIRE_DATE;
	private static final String SALT = "OSF";
	
	//토큰값 생성
	public String makeJWT(UserVO user) {
		Calendar calendar = Calendar.getInstance();
		ISSUE_DATE = calendar.getTime();
		calendar.add(Calendar.MONTH, 1);
		EXPIRE_DATE = calendar.getTime();
		String fuToken = JWT.create().withIssuer(user.getFuId()).withIssuedAt(ISSUE_DATE).withExpiresAt(EXPIRE_DATE).sign(Algorithm.HMAC256(SALT));
		log.debug("jwt=>{}",fuToken);
		return fuToken;
	}
	//토큰값 비교
	public void checkJWT(String fuToken, String fuId) {
		JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SALT)).withIssuer(fuId).build();
		log.info("rl=>{}", verifier);
		DecodedJWT decode = verifier.verify(fuToken);
		log.info("decode=>{}",decode);
		log.info("issuer=>{}",decode.getIssuer());
		log.info("issue date=>{}",decode.getIssuedAt());
		log.info("expired date=>{}",decode.getExpiresAt());
	}
}
