package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Alias("itemReview")
public class ItemReviewVO {
	
	private Integer itemReviewNum;

	private Integer itemReviewScore;
	private String itemReviewContent;
	private Integer itemReviewReport;
	private String itemReviewDate;
	private String itemReviewTime;
	private String itemReviewUpdatedDate;
	private String itemReviewUpdatedTime;
	private String itemReviewPath1;
	private String itemReviewPath2;
	
	private MultipartFile fpFile;
	
	private String  fuId;

	private String fuName; 		
    private String fuTrans;		
	private String fuAge; 			
	private String fuSkinType; 			
	private String fuSkinIssue; 	
	
	private String ageg;
	
	
	private Integer fpNum;
	



}
