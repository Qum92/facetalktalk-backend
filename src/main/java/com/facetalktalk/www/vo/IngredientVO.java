package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("ingredient")
public class IngredientVO {
	private String fiNum;
	private String fiSafeLevel;
	private String fiKorName;
	private String fiEngName;
	private String fiDetail;
	private Integer fpNum;

}
