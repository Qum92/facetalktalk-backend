package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Alias("inquiry")
public class InquiryVO {
	
	private Integer ciNum;
	private String ciTitle;
	private String ciId;
	private String ciCarte;
	private String ciContent;
	private String ciCredat;
	private String ciCretim;
	private String ciFileName;
	private MultipartFile ciFile;
}
