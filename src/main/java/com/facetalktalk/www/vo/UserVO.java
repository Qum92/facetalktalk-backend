package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("user")
public class UserVO {
	private Integer fuNum;
	private String fuId;
	private String fuName;
	private String fuPwd;
	private String fuZipcode;
	private String fuAddr;
	private String fuAddrDetail;
	private String fuMobile;
	private String fuEmail;
	private String fuCredat;
	private String fuCretim;
	private String fuTrans;
	private String fuAge;
	private String fuSkinType;
	private String fuSkinIssue;
	private String fuChild;
	private String fuUpdat;
	private String fuUptim;
	private String fuToken;
}
