package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Alias("board")
public class BoardVO {
	private Integer fbNum;
	private String fbCarte;
	private String fbId;
	private String fbTitle;
	private String fbContent;
	private String fbCredat;
	private String fbCretim;
	private Integer fbCnt;
	private Integer fbLike;
	private String fbFileName;
	private String fbFilePath;
	private MultipartFile fbFile;
	private String fbUpdat;
	private String fbUptim;
}
