package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("product")
public class ProductVO {
	private Integer fpNum;
	private String fpName;
	private String fpVolume;
	private Integer fpPrice;
	private String fpCarte;
	private String fpSeller;
	private String fpDetail;
	private String fpFileName;
	
	// 리뷰갯슈, 평점용 가상컬럼
	private Integer itemReviewCount;
	private Integer itemReviewAvgscore;	
	
	// 가격조회 필터용 가상컬럼
	private Integer minPrice;
	private Integer maxPrice;
	
}
