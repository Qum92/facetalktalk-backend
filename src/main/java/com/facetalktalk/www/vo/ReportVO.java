package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Alias("report")
public class ReportVO {
	private Integer crNum;
	private String crTitle;
	private String crId;
	private String crCarte;
	private String crContent;
	private String crCredat;
	private String crCretim;
	private String crFileName; 
	private MultipartFile crFile;
}
