package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("sample")
public class SampleVO {
	
	private Integer thNum;
	private String thSource;
	private String thTarget;
	private String thText;
	
}
