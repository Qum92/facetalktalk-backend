package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("userAdd")
public class UserAddVO {
	private Integer fuNum;
	private String fuId;
	private String fuTrans;
	private String fuAge;
	private String fuSkinType;
	private String fuSkinIssue;
	private String fuChild;
	private String fuUpdat;
	private String fuUptim;
	private String fuToken;
}
