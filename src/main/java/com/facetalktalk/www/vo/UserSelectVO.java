package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("userS")
public class UserSelectVO {
	private Integer fuNum;
	private String fuId;
	private String fuName;
	private String fuZipcode;
	private String fuAddr;
	private String fuAddrDetail;
	private String fuMobile;
	private String fuEmail;
	private String fuUpdat;
	private String fuUptim;
	private String fuToken;
}
