package com.facetalktalk.www.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("command")
public class CommandVO {
	private Integer bdNum;
	private String bdContent;
	private String bdCredat;
	private String bdCretim;
	private String bdUpdat;
	private String bdUptim;
	private String bdId;
	private Integer fbNum;
	private Integer bdLike;
}
