package com.facetalktalk.www.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.ProductVO;
@Mapper
public interface HomeMapper {
	public List<ProductVO> selectCategory(ProductVO product);
	// 홈 화면 검색 버튼
	public List<ProductVO> fpnameSearch(ProductVO product);

}
