package com.facetalktalk.www.mapper;


import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.InquiryVO;
import com.facetalktalk.www.vo.ItemReviewVO;
import com.facetalktalk.www.vo.ReportVO;
import com.facetalktalk.www.vo.UserAddVO;
import com.facetalktalk.www.vo.UserSelectVO;

@Mapper
public interface MyPageMapper {
	// 회원정보 수정 정보가져오기
	public UserSelectVO selectUserInfo(UserSelectVO userS);
	// 회원정보 추가정보(피부타입)  가져오기
	public UserAddVO selectAddUserInfo(UserAddVO userAdd);

	
	// updateUser
	public Integer UserAddUpdate(UserAddVO userAdd);
	
	// 리뷰내역
	public List<ItemReviewVO> selectReviewList(ItemReviewVO itemReview);
	// 문의 내역
	public List<InquiryVO> selectInquiryList(InquiryVO inquiry);
	// 신고 내역
	public List<ReportVO> selectReportList(ReportVO report);


}
