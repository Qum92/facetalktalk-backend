package com.facetalktalk.www.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.CommandVO;

@Mapper
public interface CommandMapper {
	//게시물 댓글 조회
	public List<CommandVO> selectCommandByfbNum(Integer fbNum);
	//게시물 댓글 등록
	public Integer insertCommand(CommandVO command);
	//게시물 댓글 업데이트
	public Integer updateCommand(CommandVO command);
	//게시물 댓글 삭제
	public Integer deleteCommand(Integer bdNum);
	//게시물 댓글 좋아요 업데이트
	public Integer updateCommandLike(Integer bdNum);
}
