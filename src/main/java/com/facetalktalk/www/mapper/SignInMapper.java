package com.facetalktalk.www.mapper;


import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.UserVO;

@Mapper
public interface SignInMapper {
	//로그인
	public UserVO selectFuIdAndFuPwd(UserVO user);
}
