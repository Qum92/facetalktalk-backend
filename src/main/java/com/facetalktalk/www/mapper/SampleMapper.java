package com.facetalktalk.www.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.SampleVO;

@Mapper
public interface SampleMapper {

	public List<SampleVO> selectSampleList();
}
