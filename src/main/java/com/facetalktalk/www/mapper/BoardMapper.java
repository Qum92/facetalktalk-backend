package com.facetalktalk.www.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.BoardVO;

@Mapper
public interface BoardMapper {
	//자유게시판 리스트 불러오기
	public List<BoardVO> selectBoardList();
	//자유게시판 번호로 정보 불러오기
	public List<BoardVO> selectBoardByfbNum(Integer fbNum);
	//자유게시판 카테고리 불러오기
	public List<BoardVO> selectBoardCarteList();
	//자유게시판 카테고리랑 제목으로 검색
	public List<BoardVO> selectBoardByfbTitle(BoardVO board);
	//게시물 등록
	public Integer insertBoard(BoardVO board);
	//게시물 업데이트
	public Integer updateBoard(BoardVO board);
	//게시물 삭제
	public Integer deleteBoard(Integer fbNum);
	//게시물 조회수 업데이트
	public Integer updateBoardCnt(Integer fbNum);
	//게시물 좋아요 수 업데이트
	public Integer updateBoardLike(Integer fbNum);
}
