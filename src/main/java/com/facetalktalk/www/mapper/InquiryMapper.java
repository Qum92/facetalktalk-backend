package com.facetalktalk.www.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.InquiryVO;

@Mapper
public interface InquiryMapper {
	//문의 리스트 내역 조회
	public List<InquiryVO> selectInquiryList();
	//문의 카테고리 불러오기
	public List<InquiryVO> selectInquiryCarteList();
	//문의 번호로 조회
	public List<InquiryVO> selectInquiryByNum(Integer ciNum);
	//문의 등록
	public Integer insertInquiry(InquiryVO inquiry);
}
