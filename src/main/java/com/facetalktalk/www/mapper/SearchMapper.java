package com.facetalktalk.www.mapper;

import java.util.List;

import com.facetalktalk.www.vo.IngredientVO;
import com.facetalktalk.www.vo.ItemReviewVO;
import com.facetalktalk.www.vo.ProductVO;

public interface SearchMapper {

	// 상품 번호 가져오는역할
	public ProductVO getProductNum(ProductVO product);
	// 상품번호로 성분정보 가져오는역할
	public List<IngredientVO> getFaceIngredient(Integer fpNum);
	
	// 상품 후기 등록
	public Integer insertProductReview(ItemReviewVO itemReview);
	// 상품 후기 가져오기
	public List<ItemReviewVO> getItemReview(Integer fpNum);
	
	// 상품 가격 필터
	public List<ProductVO> filterPrice(ProductVO product);

	// 상품 후기(리뷰) 필터
	public List<ItemReviewVO> filterReview(ItemReviewVO itemReview);
	
	// 상품 리뷰 중복작성여부 확인 
//	public Integer selectReviewOverlap(String fuId, String fpNum);
	public ItemReviewVO selectReviewOverlap( ItemReviewVO itemReview);

	
}
