package com.facetalktalk.www.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.facetalktalk.www.vo.UserVO;

@Mapper
public interface SignUpMapper {
	//회원리스트 조회
	public List<UserVO> selectFaceUserList();
	//회원가입시 아이디조회
	public Integer selectByFuId(String fuId);
	//회원가입
	public Integer insertFaceUser(UserVO user);
	//회원정보 수정
	public Integer updateFaceUser(UserVO user);
	//회원정보 삭제
	public Integer deleteFaceUser(String fuId);
}
