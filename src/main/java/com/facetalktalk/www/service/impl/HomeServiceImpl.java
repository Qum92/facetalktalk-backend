package com.facetalktalk.www.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.mapper.HomeMapper;
import com.facetalktalk.www.service.HomeService;
import com.facetalktalk.www.vo.ProductVO;
@Service
public class HomeServiceImpl implements HomeService {

	@Resource
	private HomeMapper homeMapper;
	
	@Override
	public List<ProductVO> selectCategory(ProductVO product) {
		return homeMapper.selectCategory(product);
	}

	@Override
	public List<ProductVO> fpnameSearch(ProductVO product) {
		return homeMapper.fpnameSearch(product);
	}

}
