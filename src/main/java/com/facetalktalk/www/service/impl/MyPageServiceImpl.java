package com.facetalktalk.www.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.mapper.MyPageMapper;
import com.facetalktalk.www.service.MyPageService;
import com.facetalktalk.www.vo.InquiryVO;
import com.facetalktalk.www.vo.ItemReviewVO;
import com.facetalktalk.www.vo.ReportVO;
import com.facetalktalk.www.vo.UserAddVO;
import com.facetalktalk.www.vo.UserSelectVO;
@Service

public class MyPageServiceImpl implements MyPageService {

	@Resource
	private MyPageMapper mpm;
	
	@Override
	public UserSelectVO selectUserInfo(UserSelectVO userS) {
		return mpm.selectUserInfo(userS);
	}

	@Override
	public UserAddVO selectAddUserInfo(UserAddVO userAdd) {
		return mpm.selectAddUserInfo(userAdd);
	}

	@Override
	public Integer UserAddUpdate(UserAddVO userAdd) {
		return mpm.UserAddUpdate(userAdd);
	}

	@Override
	public List<ItemReviewVO> selectReviewList(ItemReviewVO itemReview) {
		return mpm.selectReviewList(itemReview);
	}

	@Override
	public List<InquiryVO> selectInquiryList(InquiryVO inquiry) {
		return mpm.selectInquiryList(inquiry);
	}

	@Override
	public List<ReportVO> selectReportList(ReportVO report) {
		return mpm.selectReportList(report);
	}


}
