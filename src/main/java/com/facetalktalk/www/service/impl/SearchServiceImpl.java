package com.facetalktalk.www.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.facetalktalk.www.mapper.SearchMapper;
import com.facetalktalk.www.service.SearchService;
import com.facetalktalk.www.vo.IngredientVO;
import com.facetalktalk.www.vo.ItemReviewVO;
import com.facetalktalk.www.vo.ProductVO;
@Service
public class SearchServiceImpl implements SearchService {
	@Resource
	private SearchMapper searchMapper;
	@Override
	public ProductVO getProductNum(ProductVO product) {
		return searchMapper.getProductNum(product);
	}
	@Override
	public List<IngredientVO> getFaceIngredient(Integer fpNum) {
		return searchMapper.getFaceIngredient(fpNum);
	}
	// 상품 후기 등록
	@Override
	public Integer insertProductReview(ItemReviewVO itemReview) {
		String filePath ="C:\\Users\\Administrator\\git\\facetalktalk\\src\\main\\webapp\\resources\\review\\imgs\\";
		if(itemReview.getFpFile()!=null) {
			MultipartFile ItemReviewProfileFile = itemReview.getFpFile();
			String fileName = ItemReviewProfileFile.getOriginalFilename();
			String extName = FilenameUtils.getExtension(fileName);
			String reName = Long.toString(System.nanoTime());
			reName += "." +extName;
			File targetFile = new  File(filePath+reName);
			try {
				Files.copy(ItemReviewProfileFile.getInputStream(), targetFile.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
			itemReview.setItemReviewPath1("/imgs/" + reName);;
		}
		return searchMapper.insertProductReview(itemReview);
	}
	// 상품 후기 가져오기
	@Override
	public List<ItemReviewVO> getItemReview(Integer fpNum) {
		return searchMapper.getItemReview(fpNum);
	}
	// 가격 필터   public Integer filterPrice(ProductVO product);
	@Override
	public List<ProductVO> filterPrice(ProductVO product) {
		return searchMapper.filterPrice(product);
	}
	// 상품 후기(리뷰) 필터
	@Override
	public List<ItemReviewVO> filterReview(ItemReviewVO itemReview) {
		return searchMapper.filterReview(itemReview);
	}
	// 상품 리뷰 중복작성여부 확인 
	@Override
	public ItemReviewVO selectReviewOverlap(ItemReviewVO itemReview) {
		return searchMapper.selectReviewOverlap(itemReview);
	}

	

}
