package com.facetalktalk.www.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.common.FileUpload;
import com.facetalktalk.www.mapper.BoardMapper;
import com.facetalktalk.www.service.BoardService;
import com.facetalktalk.www.vo.BoardVO;

@Service
public class BoardServiceImpl implements BoardService {
	@Resource
	private BoardMapper bm;
	@Resource
	private FileUpload fu;
	//자유게시판 리스트 불러오기
	public List<BoardVO> selectBoardList() {
		return bm.selectBoardList();
	}
	//자유게시판 번호로 정보 불러오기 및 조회수 업데이트
	public List<BoardVO> selectBoardByfbNum(Integer fbNum) {
		List<BoardVO> fbList = bm.selectBoardByfbNum(fbNum);
		if(fbList!=null) {
			bm.updateBoardCnt(fbNum);
		}
		return fbList;
	}
	//자유게시판 사진 등록 및 게시물 등록
	public Integer insertBoard(BoardVO board) {
		String filePath = "D:\\study\\workspace\\facetalktalk\\src\\main\\webapp\\resource\\board\\img";
		if(board.getFbFile()!=null) {
			String fbFileName = fu.fileUpload(board.getFbFile(),filePath);
			board.setFbFileName(fbFileName);
			board.setFbFilePath(filePath);
		}
		return bm.insertBoard(board);
	}
	//게시물 업데이트
	public Integer updateBoard(BoardVO board) {
		if(board.getFbLike()!=null) {
			return bm.updateBoardLike(board.getFbNum());
		}
		if(board.getFbFile()!=null) {
			String filePath = "D:\\study\\workspace\\facetalktalk\\src\\main\\webapp\\resource\\board\\img";
			String fbFileName = fu.fileUpload(board.getFbFile(),filePath);
			board.setFbFileName(fbFileName);
			board.setFbFilePath(filePath);
			return bm.updateBoard(board);
		}
		return bm.updateBoard(board);
	}
	//게시물 삭제
	public Integer deleteBoard(Integer fbNum) {
		return bm.deleteBoard(fbNum);
	}
	//자유게시판 카테고리 불러오기
	public List<BoardVO> selectBoardCarteList() {
		return bm.selectBoardCarteList();
	}
	//자유게시판 카테고리랑 제목으로 검색
	public List<BoardVO> selectBoardByfbTitle(String fbCarte, String fbTitle){
		BoardVO board = new BoardVO();
		board.setFbCarte(fbCarte);
		board.setFbTitle(fbTitle);
		return bm.selectBoardByfbTitle(board);
	}

}
