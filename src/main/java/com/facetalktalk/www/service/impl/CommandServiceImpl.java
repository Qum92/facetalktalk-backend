package com.facetalktalk.www.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.mapper.CommandMapper;
import com.facetalktalk.www.service.CommandService;
import com.facetalktalk.www.vo.CommandVO;

@Service
public class CommandServiceImpl implements CommandService {
	@Resource
	private CommandMapper cm;
	//게시물 댓글 조회
	public List<CommandVO> selectCommandByfbNum(Integer fbNum) {
		return cm.selectCommandByfbNum(fbNum);
	}
	//게시물 댓글 등록
	public Integer insertCommand(CommandVO command) {
		return cm.insertCommand(command);
	}
	//게시물 댓글 업데이트
	public Integer updateCommand(CommandVO command) {
		if(command.getBdLike()!=null) {
			return cm.updateCommandLike(command.getBdNum());
		}
		return cm.updateCommand(command);
	}
	//게시물 댓글 삭제
	public Integer deleteCommand(Integer bdNum) {
		return cm.deleteCommand(bdNum);
	}
	//게시물 댓글 좋아요 업데이트
	public Integer updateCommandLike(Integer bdNum) {
		return cm.updateCommandLike(bdNum);
	}

}
