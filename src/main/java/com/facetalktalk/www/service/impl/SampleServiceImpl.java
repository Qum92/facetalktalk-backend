//package com.facetalktalk.www.service.impl;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//
//import org.springframework.stereotype.Service;
//
//import com.facetalktalk.www.mapper.SampleMapper;
//import com.facetalktalk.www.service.SampleService;
//import com.facetalktalk.www.vo.SampleVO;
//
//
//
//@Service
//public class SampleServiceImpl implements SampleService {
//
//	@Resource
//	private SampleMapper sampleMapper;
//
//	@Override
//	public List<SampleVO> selectSample() {
//		return sampleMapper.selectSampleList();
//	}
//
//}
