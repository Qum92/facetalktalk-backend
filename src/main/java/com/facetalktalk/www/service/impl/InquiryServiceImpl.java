package com.facetalktalk.www.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.common.FileUpload;
import com.facetalktalk.www.mapper.InquiryMapper;
import com.facetalktalk.www.service.InquiryService;
import com.facetalktalk.www.vo.InquiryVO;

@Service
public class InquiryServiceImpl implements InquiryService {
	@Resource
	private InquiryMapper im;
	@Resource
	private FileUpload fu;
	//문의 리스트 내역 조회
	public List<InquiryVO> selectInquiryList() {
		return im.selectInquiryList();
	}
	//문의 카테고리 불러오기
	public List<InquiryVO> selectInquiryCarteList() {
		return im.selectInquiryCarteList();
	}
	//문의 번호로 조회
	public List<InquiryVO> selectInquiryByNum(Integer ciNum) {
		return im.selectInquiryByNum(ciNum);
	}
	//문의 등록
	public Integer insertInquiry(InquiryVO inquiry) {
		String filePath = "D:\\study\\workspace\\facetalktalk\\src\\main\\webapp\\resources\\customer\\inquiry\\imgs";
		if(inquiry.getCiFile()!=null) {
			String ciFileName = fu.fileUpload(inquiry.getCiFile(), filePath);
			inquiry.setCiFileName(ciFileName);
		}
		return im.insertInquiry(inquiry);
	}

}
