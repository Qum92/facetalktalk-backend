package com.facetalktalk.www.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.common.MakeJWT;
import com.facetalktalk.www.common.SHAEncoder;
import com.facetalktalk.www.mapper.SignInMapper;
import com.facetalktalk.www.service.SignInService;
import com.facetalktalk.www.vo.UserVO;

@Service
public class SignInServiceImpl implements SignInService {
	@Resource
	private SignInMapper sm;
	@Resource
	private SHAEncoder shae;
	@Resource
	private MakeJWT jwt;
	//로그인
	public UserVO selectFuIdAndFuPwd(UserVO user) {
		String fuPwd = shae.encode(user.getFuPwd());
		user.setFuPwd(fuPwd);
		UserVO temp = sm.selectFuIdAndFuPwd(user);
		if(temp!=null) {
			if(temp.getFuId().equals(user.getFuId())){
				temp.setFuToken(jwt.makeJWT(user));
				return temp;
			}
		}
		return null;
		
//		return sm.selectFuIdAndFuPwd(user);  
	}

}
