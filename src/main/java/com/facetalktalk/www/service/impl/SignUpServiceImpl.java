package com.facetalktalk.www.service.impl;

import java.util.List;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.common.SHAEncoder;
import com.facetalktalk.www.mapper.SignUpMapper;
import com.facetalktalk.www.service.SignUpService;
import com.facetalktalk.www.vo.UserVO;

@Service
public class SignUpServiceImpl implements SignUpService {
	@Resource
	private SignUpMapper um;
	@Resource
	private SHAEncoder shae;
	//회원리스트 조회
	public List<UserVO> selectFaceUserList() {
		return um.selectFaceUserList();
	}
	//회원가입시 아이디조회
	public Integer selectByFuId(String fuId) {
		return um.selectByFuId(fuId);
	}
	//회원가입
	public Integer insertFaceUser(UserVO user) {
		String fuPwd = shae.encode(user.getFuPwd());
		user.setFuPwd(fuPwd);
		return um.insertFaceUser(user);
	}
	//회원정보 수정
	public Integer updateFaceUser(UserVO user) {
		if(user.getFuPwd()!=null) {
			String fuPwd = shae.encode(user.getFuPwd());
			user.setFuPwd(fuPwd);
		}
		return um.updateFaceUser(user);
	}
	//회원정보 삭제
	public Integer deleteFaceUser(String fuId) {
		return um.deleteFaceUser(fuId);
	}

	

}
