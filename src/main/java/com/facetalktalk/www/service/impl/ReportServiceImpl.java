package com.facetalktalk.www.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.facetalktalk.www.common.FileUpload;
import com.facetalktalk.www.mapper.ReportMapper;
import com.facetalktalk.www.service.ReportService;
import com.facetalktalk.www.vo.ReportVO;

@Service
public class ReportServiceImpl implements ReportService{
	@Resource
	private ReportMapper rm;
	@Resource
	private FileUpload fu;
	//신고 내역 조회
	public List<ReportVO> selectReportList() {
		return rm.selectReportList();
	}
	//신고 카테고리 불러오기
	public List<ReportVO> selectReportCarteList() {
		return rm.selectReportCarteList();
	}
	//신고 번호로 조회
	public List<ReportVO> selectReportByNum(Integer crNum) {
		return rm.selectReportByNum(crNum);
	}
	//신고하기 등록
	public Integer insertReport(ReportVO report) {
		String filePath = "D:\\study\\workspace\\facetalktalk\\src\\main\\webapp\\resources\\customer\\report\\imgs";
		if(report.getCrFile()!=null) {
			String crFileName = fu.fileUpload(report.getCrFile(), filePath);
			report.setCrFileName(crFileName);
		}
		return rm.insertReport(report);
	}
	
	
}
