package com.facetalktalk.www.service;

import java.util.List;

import com.facetalktalk.www.vo.ProductVO;

public interface HomeService {
	public List<ProductVO> selectCategory(ProductVO product);
	// 홈 화면 검색 버튼
	public List<ProductVO> fpnameSearch(ProductVO product);
}
