package com.facetalktalk.www.service;

import java.util.List;

import com.facetalktalk.www.vo.CommandVO;

public interface CommandService {
	//게시물 댓글 조회
	public List<CommandVO> selectCommandByfbNum(Integer fbNum);
	//게시물 댓글 등록
	public Integer insertCommand(CommandVO command);
	//게시물 댓글 업데이트
	public Integer updateCommand(CommandVO command);
	//게시물 댓글 삭제
	public Integer deleteCommand(Integer bdNum);
	//게시물 댓글 좋아요 업데이트
	public Integer updateCommandLike(Integer bdNum);
}
