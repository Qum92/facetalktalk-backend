package com.facetalktalk.www.service;

import java.util.List;

import com.facetalktalk.www.vo.BoardVO;

public interface BoardService {
	//자유게시판 리스트 불러오기
	public List<BoardVO> selectBoardList();
	//자유게시판 카테고리 불러오기
	public List<BoardVO> selectBoardCarteList();
	//자유게시판 번호로 정보 불러오기
	public List<BoardVO> selectBoardByfbNum(Integer fbNum);
	//자유게시판 카테고리랑 제목으로 검색
	public List<BoardVO> selectBoardByfbTitle(String fbCarte, String fbTitle);
	//게시물 등록
	public Integer insertBoard(BoardVO board);
	//게시물 업데이트
	public Integer updateBoard(BoardVO board);
	//게시물 삭제
	public Integer deleteBoard(Integer fbNum);
}
