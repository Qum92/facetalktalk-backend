package com.facetalktalk.www.service;

import java.util.List;

import com.facetalktalk.www.vo.ReportVO;

public interface ReportService {
	//신고 내역 조회
	public List<ReportVO> selectReportList();
	//신고 카테고리 불러오기
	public List<ReportVO> selectReportCarteList();
	//신고 번호로 조회
	public List<ReportVO> selectReportByNum(Integer crNum);
	//신고하기 등록
	public Integer insertReport(ReportVO report);

}
