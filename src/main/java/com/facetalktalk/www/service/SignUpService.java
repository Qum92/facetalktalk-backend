package com.facetalktalk.www.service;

import java.util.List;

import com.facetalktalk.www.vo.UserVO;

public interface SignUpService {
	//회원리스트 조회
	public List<UserVO> selectFaceUserList();
	//회원가입시 아이디조회
	public Integer selectByFuId(String fuId);
	//회원가입
	public Integer insertFaceUser(UserVO user);
	//회원정보 수정
	public Integer updateFaceUser(UserVO user);
	//회원정보 삭제
	public Integer deleteFaceUser(String fuId);
}
