package com.facetalktalk.www.advise;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Slf4j
public class SampleAdvice {
	
	//해당 메소드 실행시 로그
	@Before("execution(* com.facetalktalk.www.controller.*.*(..))")
	public void beforeExecute(JoinPoint joinPoint) {
		log.info("execution Method is =>{}", joinPoint);
	}
}
