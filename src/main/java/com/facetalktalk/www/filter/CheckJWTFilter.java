package com.facetalktalk.www.filter;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.filter.GenericFilterBean;

import com.facetalktalk.www.common.MakeJWT;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CheckJWTFilter extends GenericFilterBean{
	@Resource
	private MakeJWT mjwt;
	private static final String[] EXCLUDE_URI = {"signin","signup"};
	public boolean CheckUri(String uri) {
		for(String excludeUri : EXCLUDE_URI) {
			if(uri.indexOf(excludeUri)!=-1) {
				return false;
			}
		}
		return true;
	}
	//프론트에서 토큰 값이 없을시에 서버에 들어오는 접근을 차단
	//미구현
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest)request;
		String method = req.getMethod();
		if(!method.equals("OPTIONS") &&
		   !req.getRequestURI().equals("/signin") &&
		   !req.getRequestURI().equals("/boardList") &&
		   !req.getRequestURI().equals("/product") &&
		   !req.getRequestURI().equals("/review") &&
		   !req.getRequestURI().equals("/signup")) {
			String fuId = req.getHeader("X-AUTH-ID");
			String fuToken = req.getHeader("X-AUTH-TOKEN");
			try {
				mjwt.checkJWT(fuToken, fuId);
			}catch(Exception e) {
				log.error("JWT error => {}",e);
				throw new ServletException("토큰키가 올바르지 않습니다.");
			}
		}
		chain.doFilter(request, response);
	}
}
