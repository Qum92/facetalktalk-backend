package com.facetalktalk.www.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.facetalktalk.www.common.FileUpload;
import com.facetalktalk.www.service.BoardService;
import com.facetalktalk.www.vo.BoardVO;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin("*")
@Controller
@Slf4j
public class BoardController {
	@Resource
	private BoardService bs;
	@Resource
	private FileUpload fu;
	//자유게시판 파일 경로
	final String FILE_PATH = "D:\\study\\workspace\\facetalktalk\\src\\main\\webapp\\resources\\board\\img";
	//자유게시판 리스트 불러오기
	@GetMapping("/boardList")
	public @ResponseBody List<BoardVO> selectBoardList() {
		return bs.selectBoardList();
	}
	//자유게시판 카테고리 불러오기
	@GetMapping("/boardCarteList")
	public @ResponseBody List<BoardVO> selectBoardCarteList() {
		return bs.selectBoardCarteList();
	}
	//자유게시판 번호로 정보 불러오기
	@GetMapping("/board/{fbNum}")
	public @ResponseBody List<BoardVO> selectBoardByfbNum(@PathVariable Integer fbNum) {
		log.debug("fbNum=>{}", fbNum);
		return bs.selectBoardByfbNum(fbNum);
	}
	//자유게시판 카테고리랑 제목으로 검색
	@GetMapping("/board/{fbCarte}/{fbTitle}")
	public @ResponseBody List<BoardVO> selectBoardByfbTitle(@PathVariable String fbCarte,
			@PathVariable String fbTitle) {
		log.debug("fbCarte=>{}", fbCarte);
		log.debug("fbTitle=>{}", fbTitle);
		return bs.selectBoardByfbTitle(fbCarte, fbTitle);
	}
	//게시물 등록
	@PostMapping("/board")
	public @ResponseBody Integer insertBoard(@ModelAttribute BoardVO board) {
		log.debug("board=>{}", board);
		return bs.insertBoard(board);
	}
	//게시물 파일 업로드 저장
	@PostMapping("/upload/board/img")
	public @ResponseBody Map<String, Object> upload(MultipartFile upload) throws IllegalStateException, IOException {
		String reName = fu.fileUpload(upload, FILE_PATH + File.separator);
		Map<String, Object> map = new HashMap<>();
		map.put("url", "http://localhost:89/imaged?name=" + reName);
		map.put("uploaded", 1);
		map.put("uploadedPercent", 1);
		map.put("error", "error");
		return map;
	}
	//게시물 사진 조회
	@GetMapping("/imaged")
	public ResponseEntity<byte[]> image(@RequestParam(value = "name") String name) throws IOException {
		File file = new File(FILE_PATH + File.separator + name);
		System.out.println("file" + file);
		FileInputStream fi = new FileInputStream(file);
		byte[] buffer = new byte[fi.available()];
		fi.read(buffer);
		fi.close();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_PNG);
		return new ResponseEntity<byte[]>(buffer, headers, HttpStatus.OK);
	}
	//게시물 업데이트
	@PostMapping("/boardup")
	public @ResponseBody Integer updateBoard(@ModelAttribute BoardVO board) {
		log.debug("board=>{}", board);
		return bs.updateBoard(board);
	}
	//게시물 삭제
	@DeleteMapping("/board/{fbNum}")
	public @ResponseBody Integer deleteBoard(@PathVariable Integer fbNum) {
		log.debug("fbNum=>{}", fbNum);
		return bs.deleteBoard(fbNum);
	}
}