package com.facetalktalk.www.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.facetalktalk.www.service.InquiryService;
import com.facetalktalk.www.vo.InquiryVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin("*")
public class InquiryController {
	@Resource
	private InquiryService is;
	//문의 리스트 내역 조회
	@GetMapping("/inquiryList")
	public List<InquiryVO> selectInquiryList(){
		return is.selectInquiryList();
	}
	//문의 카테고리 불러오기
	@GetMapping("/inquiryCarteList")
	public List<InquiryVO> selectInquiryCarteList(){
		return is.selectInquiryCarteList();
	}
	//문의 번호로 조회
	@GetMapping("/inquiry/{ciNum}")
	public List<InquiryVO> selectInquiryByNum(@PathVariable Integer ciNum){
		log.debug("ciNum=>{}",ciNum);
		return is.selectInquiryByNum(ciNum);
	}
	//문의 등록
	@PostMapping("/inquiry")
	public Integer insertInquiry(@ModelAttribute InquiryVO inquiry){
		log.debug("inquiry=>{}",inquiry);
		return is.insertInquiry(inquiry);
	}
}
