package com.facetalktalk.www.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.facetalktalk.www.service.HomeService;
import com.facetalktalk.www.vo.ProductVO;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin("*")
@Controller
@Slf4j
public class HomeController {  
 
	@Resource
	private HomeService homeService;
	
	/*
	 * 카테고리 검색 
	 * fpCarte 
	 * ProductVO product
	 */
	@GetMapping("/select/category/{fpCarte}")
	public @ResponseBody List<ProductVO> selectCategory(ProductVO product) {
//		log.info("product=>{}", product);
		return homeService.selectCategory(product);
	}
	
	// 홈 화면 검색
	// public List<ProductVO> fpnameSearch(ProductVO product);
	@GetMapping("/fpname/search/{fpName}")
	public @ResponseBody List<ProductVO> fpnameSearch(ProductVO product){
		log.info("product=>{}", product);
		return homeService.fpnameSearch(product);
	}
	
	
	
	

}


