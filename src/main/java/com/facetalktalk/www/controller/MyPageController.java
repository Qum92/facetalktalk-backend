package com.facetalktalk.www.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.facetalktalk.www.service.MyPageService;
import com.facetalktalk.www.vo.InquiryVO;
import com.facetalktalk.www.vo.ItemReviewVO;
import com.facetalktalk.www.vo.ReportVO;
import com.facetalktalk.www.vo.UserAddVO;
import com.facetalktalk.www.vo.UserSelectVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin("*")
public class MyPageController {
	@Resource
	private MyPageService mps;

	// 회원정보 수정 정보가져오기
	// UserVO user
	@GetMapping("/user/{fuId}")
	public @ResponseBody UserSelectVO selectUserInfo(UserSelectVO userS) {
		return mps.selectUserInfo(userS);
	}

	// 회원정보 추가정보(피부타입) 가져오기
	// UserVO user
	@GetMapping("/user/add/{fuId}")
	public @ResponseBody UserAddVO selectAddUserInfo(UserAddVO userAdd) {
		return mps.selectAddUserInfo(userAdd);
	}

	// 회원정보 추가정보(피부타입) 수정
	// updateUser
	@PutMapping("/useradd/update")
	public @ResponseBody Integer UserAddUpdate(@RequestBody UserAddVO userAdd) {
		log.info("user =>{}", userAdd);
		return mps.UserAddUpdate(userAdd);
	}

	// 리뷰내역
	//	public List<ItemReviewVO> selectReviewList(ItemReviewVO itemReview);
	@GetMapping("/reviewList/{fuId}")
	public @ResponseBody List<ItemReviewVO> selectReviewList(ItemReviewVO itemReview){
		log.info("itemReview=>{}", itemReview);
		return mps.selectReviewList(itemReview);
	}
	
	// 문의 내역
	//	public List<InquiryVO> selectInquiryList(InquiryVO inquiry);
	@GetMapping("/inquiry/select/{ciId}")
	public @ResponseBody List<InquiryVO> selectInquiryList(InquiryVO inquiry){
		log.info("inquiry=>{}", inquiry);
		return mps.selectInquiryList(inquiry);
	}

	// 신고 내역
	//	public List<ReportVO> selectReportList(ReportVO report);
	@GetMapping("/report/select/{crId}")
	public @ResponseBody List<ReportVO> selectReportList(ReportVO report){
		log.info("report=>{}", report);
		return mps.selectReportList(report);
	}

//	@PostMapping("/signin")
//	public @ResponseBody UserVO selectFuIdAndFuPwd(@RequestBody UserVO user) {
//		log.debug("user=>{}",user);
//		return sis.selectFuIdAndFuPwd(user);
//	}

}
