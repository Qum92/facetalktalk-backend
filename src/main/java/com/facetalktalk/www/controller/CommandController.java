package com.facetalktalk.www.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.facetalktalk.www.service.CommandService;
import com.facetalktalk.www.vo.CommandVO;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
@CrossOrigin("*")
public class CommandController {
	@Resource
	private CommandService cs;
	//게시물 댓글 조회
	@GetMapping("/command/{fbNum}")
	public @ResponseBody List<CommandVO> selectCommandByfbNum(@PathVariable Integer fbNum){
		log.debug("fbNum=>{}",fbNum);
		return cs.selectCommandByfbNum(fbNum);
	}
	//게시물 댓글 등록
	@PostMapping("/command")
	public @ResponseBody Integer insertCommand(@RequestBody CommandVO command){
		log.debug("command=>{}",command);
		return cs.insertCommand(command);
	}
	//게시물 댓글 업데이트
	@PutMapping("/command")
	public @ResponseBody Integer updateCommand(@RequestBody CommandVO command){
		log.debug("command=>{}",command);
		return cs.updateCommand(command);
	}
	//게시물 댓글 삭제
	@DeleteMapping("/command/{bdNum}")
	public @ResponseBody Integer deleteCommand(@PathVariable Integer bdNum) {
		log.debug("bdNum=>{}",bdNum);
		return cs.deleteCommand(bdNum);
	}
	//게시물 댓글 좋아요 업데이트
	@PutMapping("/command/{bdNum}")
	public @ResponseBody Integer updateCommandLike(@PathVariable Integer bdNum) {
		return cs.updateCommandLike(bdNum);
	}
}
