package com.facetalktalk.www.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.facetalktalk.www.service.SearchService;
import com.facetalktalk.www.vo.IngredientVO;
import com.facetalktalk.www.vo.ItemReviewVO;
import com.facetalktalk.www.vo.ProductVO;

import lombok.extern.slf4j.Slf4j;

@CrossOrigin("*")
@Controller
@Slf4j
public class SearchController {

	@Resource
	private SearchService searchService;
	
	/*
	 * 번호로 제품 상세정보 가져오기
	 * fpCarte 
	 * ProductVO product
	 */
	@GetMapping("/product/{fpNum}")
	public @ResponseBody ProductVO getProductNum(ProductVO product) {
//		log.info("product => {} ", product);
		return searchService.getProductNum(product);
	}
	/*
	 * 성분정보 가져오기 
	 * IngredientVO ingredient
	 * getFaceIngredient
	 */
	@GetMapping("/ingredient/{fpNum}")
	public @ResponseBody List<IngredientVO> getIngredient(@PathVariable Integer fpNum) {
//		log.info("ingredient => {} ", fpNum);
		return searchService.getFaceIngredient(fpNum);
	}
	
	/* 상품 후기 리뷰 등록
	 * insertProductReview
	 * ItemReviewVO itemReview
	 */
	@PostMapping("/review/insert")
	public @ResponseBody Integer insertProductReview(@ModelAttribute ItemReviewVO itemReview) {
		log.info("itemReview =>{}", itemReview);
		return searchService.insertProductReview(itemReview);
	}
	
	/* 	상품 후기 가져오기
	 * 								getItemReview
	 * 	ItemReviewVO itemReview
	 */
	@GetMapping("/review/select/{fpNum}")
	public @ResponseBody List<ItemReviewVO> getItemReview(@PathVariable Integer fpNum) {
//		log.info("fpNum =>{}", fpNum);
		return searchService.getItemReview(fpNum);
	}
	// 상품 정보 가격 필터
	@GetMapping("/select/category/{fpCarte}/{minPrice}/{maxPrice}")
	public @ResponseBody List<ProductVO> filterPrice(ProductVO product) {
		return searchService.filterPrice(product);
	}    
	// 상품 후기(리뷰) 필터
	//ItemReviewVO itemReview
	// 디폴트가 @ModelAttribute  이건 폼데이터 전송할떄 사용
	@GetMapping("/review/select/filter")
	public @ResponseBody List<ItemReviewVO> filterReview(ItemReviewVO itemReview) {
		log.info("itemReview =>{}", itemReview);
		return searchService.filterReview(itemReview);
	}
	
	// 상품 리뷰 중복작성여부 확인 
	// 	public Integer selectReviewOverlap(String fuId, String fpNum);
	@GetMapping("/select/review/overlap/{fuId}/{fpNum}")
	public @ResponseBody ItemReviewVO selectReviewOverlap(ItemReviewVO itemReview) {
		log.info("itemReview =>{}", itemReview);
		return searchService.selectReviewOverlap(itemReview);
	}
	
	
}
