package com.facetalktalk.www.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.facetalktalk.www.service.SignUpService;
import com.facetalktalk.www.vo.UserVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin("*")
@Slf4j
public class SignUpContoller {
	@Resource
	private SignUpService sus;
	//회원가입시 아이디조회
	@GetMapping("/signUp/{fuId}")
	public Integer selectByFuId(@PathVariable String fuId) {
		log.debug("fuId=>{}",fuId);
		return sus.selectByFuId(fuId);
	}
	//회원리스트 조회
	@GetMapping("/signUp")
	public List<UserVO> selectFaceUserList(){
		return sus.selectFaceUserList();
	}
	//회원가입
	@PostMapping("/signUp")
	public @ResponseBody Integer insertFaceUser(@RequestBody UserVO user) {
		log.debug("user=>{}",user);
		return sus.insertFaceUser(user);
	}
	//회원정보 수정
	@PutMapping("/signUp")
	public @ResponseBody Integer updateFaceUser(@RequestBody UserVO user) {
		log.debug("user=>{}",user);
		return sus.updateFaceUser(user);
	}
	//회원정보 삭제
	@DeleteMapping("/signUp/{fuId}")
	public Integer deleteFaceUser(@PathVariable String fuId) {
		log.debug("user=>{}",fuId);
		return sus.deleteFaceUser(fuId);
	}
	
}

