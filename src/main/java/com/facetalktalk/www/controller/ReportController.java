package com.facetalktalk.www.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.facetalktalk.www.service.ReportService;
import com.facetalktalk.www.vo.ReportVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin("*")
public class ReportController {
	@Resource
	ReportService rs;
	//신고 내역 조회
	@GetMapping("/reportList")
	public List<ReportVO> selectReportList(){
		return rs.selectReportList();
	}
	//신고 카테고리 불러오기
	@GetMapping("/reportCarteList")
	public List<ReportVO> selectReportCarteList(){
		return rs.selectReportCarteList();
	}
	//신고 번호로 조회
	@GetMapping("/report/{crNum}")
	public @ResponseBody List<ReportVO> selectReportByNum(@PathVariable Integer crNum){
		log.debug("crNum=>{}",crNum);
		return rs.selectReportByNum(crNum);
	}
	//신고하기 등록
	@PostMapping("/report")
	public @ResponseBody Integer insertReport(@ModelAttribute ReportVO report){
		log.debug("report=>{}",report);
		return rs.insertReport(report);
	}
}
