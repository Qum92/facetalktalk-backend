package com.facetalktalk.www.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.facetalktalk.www.service.SignInService;
import com.facetalktalk.www.vo.UserVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@CrossOrigin("*")
public class SignInController {
	@Resource
	private SignInService sis;
	//로그인
	@PostMapping("/signin")
	public @ResponseBody UserVO selectFuIdAndFuPwd(@RequestBody UserVO user) {
		log.debug("user=>{}",user);
		return sis.selectFuIdAndFuPwd(user);
	}

}
