//package com.facetalktalk.www.controller;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import com.facetalktalk.www.service.SampleService;
//import com.facetalktalk.www.vo.SampleVO;
//
//import lombok.extern.slf4j.Slf4j;
//
//@Controller
//@Slf4j
//public class SampleTestController {
//	
//	
//	@Resource
//	private SampleService sampleService;
//	
//	@GetMapping("/test")
//	public @ResponseBody List<SampleVO> selectList(){
//		log.info("!!!!sampleController start!!!!!");
//		
//		return sampleService.selectSample();
//	}
//	
//	
//}
